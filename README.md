# Jak přidat QR Platbu do Shoptetu (bez Shoptet Pay)

**Shoptet tuto funkci už má v základu, tak tento plugin není nutný.**

1. V administraci Shoptetu v Nastavení -> Doprava a platba přidej platební metodu typu _Převodem_ s názvem např. _Převodem a QR Platba_ (název musí obsahovat slovo "Převodem"). Nezapomeň vyplnit i Možnosti dopravy a Ceník.
2. V administraci Shoptetu ve Vzhled a obsah -> Editor -> HTML kód -> Dokončená objednávka vlož tento kód a uprav data atributy, případně i styly:
```
<script src="https://janpoboril.gitlab.io/shoptet-qr-platba/checkout.js" data-account-number="222885" data-bank-code="5500" data-currency="CZK"></script>
<style>
  .qr-platba {
    margin-bottom: 3rem;
    display: flex;
    flex-wrap: wrap;
    align-items: center;
  }
  .qr-platba__qr-code {
    width: 20rem;
    height: 20rem;
  }
  .qr-platba__list {
    padding: 1rem;
  }
</style>
```

## Jak to vypadá

![QR Platba na shoptetu](screenshot.png)
