var accountNumber = document.currentScript.getAttribute('data-account-number')
var bankCode = document.currentScript.getAttribute('data-bank-code')
var currency = document.currentScript.getAttribute('data-currency')

function getQRUrl(accountNumber, bankCode, amount, currency, vs) {
    return `https://api.paylibo.com/paylibo/generator/czech/image?accountNumber=${accountNumber}&bankCode=${bankCode}&amount=${amount}&currency=${currency}&vs=${vs}`
}

document.addEventListener("DOMContentLoaded", function() {
    const recapPaymentMethod = document.querySelector('[data-testid="recapPaymentMethod"]')
    if (recapPaymentMethod && recapPaymentMethod.textContent.includes('Převodem')) {
        const price = /([\d\s]+)/.exec(document.querySelector('[data-testid="price"]').textContent)[1].replace(/\W/g, '')
        const orderId = /(\d+)/.exec(document.querySelector('[data-testid="orderNumber"]').textContent)[1]

        const block = document.createElement('div')
        block.innerHTML = `
            <div class="qr-platba">
                <img src="${getQRUrl(accountNumber, bankCode, price, currency, orderId)}" alt="QR Platba" class="qr-platba__qr-code">
                <dl class="qr-platba__list">
                    <dt>Číslo účtu:</dt>
                    <dd>${accountNumber}</dd>
                    <dt>Kód banky:</dt>
                    <dd>${bankCode}</dd>
                    <dt>Částka:</dt>
                    <dd>${price} ${currency}</dd>
                    <dt>Variabilní symbol:</dt>
                    <dd>${orderId}</dd>
                </dl>
            </div>
        `
        document.querySelector('[data-testid="recapTable"]').append(block)
    }
})
